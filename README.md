# Spring CloudKafka Streams Sample

## Prerequisites

Kafka, Zookeeper and Confluent Schema Registry

Installation using docker compose:
```
$ cd src/main/docker
$ docker-compose up -d
```   

## Overview

Example project to left join domain events on topic "domainEventFeed" (KStream) with reference data on topic "sourceReferenceData" (KTable).
The domain event feed is enriched with the reference data and output to a new topic "domainEventsEnriched".

All data is stored in Kafka in Avro format, see schemas in src/main/resources/avro.

## Test

curl --get http://localhost:8080/reference-data/source/{identification}

where {identification} is one of PAY / ORD / WAR / DEL