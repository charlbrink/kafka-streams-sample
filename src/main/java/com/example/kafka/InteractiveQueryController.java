package com.example.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.StreamsBuilderFactoryBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("reference-data")
@Slf4j
public class InteractiveQueryController {
    @Autowired
    private StreamsBuilderFactoryBean streamsBuilderFactoryBean;

    private ReadOnlyKeyValueStore<String, SourceReferenceData> keyValueStore;

    @GetMapping("/source/{identification}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> getSourceReferenceData(@PathVariable("identification") final String identification) {
        String result = "NOT FOUND";
        if (keyValueStore == null) {
            keyValueStore = streamsBuilderFactoryBean.getKafkaStreams().store(KStreamProcessorX.SOURCE_REFERENCE_STORE, QueryableStoreTypes.keyValueStore());
        }

        Optional<SourceReferenceData> sourceReferenceData = Optional.ofNullable(keyValueStore.get(identification));
        log.info("Found: [{}]", sourceReferenceData);

        if (sourceReferenceData.isPresent()) {
            result = sourceReferenceData.get().getDescription();
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
