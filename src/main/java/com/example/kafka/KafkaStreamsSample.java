package com.example.kafka;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.binding.BindingService;
import org.springframework.cloud.stream.schema.client.ConfluentSchemaRegistryClient;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.handler.annotation.SendTo;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.example.kafka.KStreamProcessorX.DOMAIN_EVENT_ENRICHED_FEED;
import static com.example.kafka.KStreamProcessorX.DOMAIN_EVENT_ENRICHED_OUTPUT;
import static com.example.kafka.KStreamProcessorX.DOMAIN_EVENT_FEED;
import static com.example.kafka.KStreamProcessorX.SOURCE_REFERENCE_DATA;
import static com.example.kafka.KStreamProcessorX.SOURCE_REFERENCE_STORE;

@Slf4j
@SpringBootApplication
@EnableBinding(KStreamProcessorX.class)
public class KafkaStreamsSample {

//    @Autowired
//    private StreamsBuilderFactoryBean streamsBuilderFactoryBean;

//    private ReadOnlyKeyValueStore<String, SourceReferenceData> keyValueStore;

    public static void main(String[] args) {
        SpringApplication.run(KafkaStreamsSample.class, args);
    }

    @StreamListener
    @SendTo(DOMAIN_EVENT_ENRICHED_OUTPUT)
    public KStream<String, DomainEventEnriched> process(@Input(DOMAIN_EVENT_FEED) KStream<String, DomainEvent> domainEventStream, @Input(SOURCE_REFERENCE_DATA) KTable<String, SourceReferenceData> sourceReferenceDataTable) {

        final Map<String, String> serdeConfig = Collections.singletonMap(
                AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        final SpecificAvroSerde<DomainEvent> domainEventSerde = new SpecificAvroSerde<>();
        domainEventSerde.configure(serdeConfig, false);

        final SpecificAvroSerde<SourceReferenceData> sourceSystemSerde = new SpecificAvroSerde<>();
        sourceSystemSerde.configure(serdeConfig, false);

        return domainEventStream.leftJoin(sourceReferenceDataTable, (domainEvent, sourceReferenceData) -> DomainEventEnriched
                .newBuilder()
                .setUuid(domainEvent.getUuid())
                .setEventType(domainEvent.getEventType())
                .setSourceIdentification(sourceReferenceData.getIdentification())
                .setSourceDescription(sourceReferenceData.getDescription())
                .build(), Joined.with(Serdes.String(), domainEventSerde, sourceSystemSerde));
    }

    @StreamListener(DOMAIN_EVENT_ENRICHED_FEED)
    public void processOutput(DomainEventEnriched value) {
        log.info("Sent: [{}]", value);
//        if (keyValueStore == null) {
//            keyValueStore = streamsBuilderFactoryBean.getKafkaStreams().store(KStreamProcessorX.SOURCE_REFERENCE_DATA, QueryableStoreTypes.keyValueStore());
//        }
//        //And lookup by key
//        log.info("Found: [{}]", Optional.ofNullable(keyValueStore.get(value.getSourceIdentification())));

    }

    @Configuration
    @EnableKafkaStreams
    static class ApplicationConfiguration {
        @Autowired
        private KafkaProperties kafkaProperties;

//        @Autowired
//        private StreamsBuilderFactoryBean streamsBuilderFactoryBean;

        @Autowired
        private BindingService bindingService;

        @Value("${spring.cloud.stream.schemaRegistryClient.endpoint}")
        private String schemaRegistryUrl;

        @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
        public StreamsConfig streamsConfigs() {
            Map<String, Object> props = new HashMap<>();
            props.put(StreamsConfig.APPLICATION_ID_CONFIG, kafkaProperties.getProperties().get(StreamsConfig.APPLICATION_ID_CONFIG));
            props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
            props.put(StreamsConfig.STATE_CLEANUP_DELAY_MS_CONFIG, kafkaProperties.getProperties().get(StreamsConfig.STATE_CLEANUP_DELAY_MS_CONFIG));
            props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, kafkaProperties.getProperties().get(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG));
            props.put(SslConfigs.SSL_PROTOCOL_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_PROTOCOL_CONFIG));
            props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG));
            props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG));
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG));
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, kafkaProperties.getProperties().get(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG));
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, kafkaProperties.getProperties().get(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG));
            return new StreamsConfig(props);
        }

        @Bean
        public GlobalKTable<String, SourceReferenceData> sourceReferenceDataTable(final StreamsBuilder builder) {
            final Map<String, String> serdeConfig = Collections.singletonMap("schema.registry.url", schemaRegistryUrl);

            final Serde<SourceReferenceData> valueSpecificAvroSerde = new SpecificAvroSerde<>();
            valueSpecificAvroSerde.configure(serdeConfig, false);
            String topicName = bindingService.getBindingServiceProperties().getBindingDestination(SOURCE_REFERENCE_DATA);
            return builder.globalTable(topicName, Consumed.with(Serdes.String(), valueSpecificAvroSerde), Materialized.<String, SourceReferenceData, KeyValueStore<Bytes, byte[]>>as(
                    SOURCE_REFERENCE_STORE).withKeySerde(Serdes.String()).withValueSerde(valueSpecificAvroSerde));
        }

//        @Bean
//        public MessageConverter domainEventMessageConverter() {
//            AvroSchemaMessageConverter converter = new AvroSchemaMessageConverter();
//            converter.setSchema(DomainEvent.SCHEMA$);
//            return converter;
//        }
//
//        @Bean
//        public MessageConverter sourceMessageConverter() {
//            AvroSchemaMessageConverter converter = new AvroSchemaMessageConverter();
//            converter.setSchema(SourceReferenceData.SCHEMA$);
//            return converter;
//        }
//
//        @Bean
//        public MessageConverter domainEventEnrichedMessageConverter() {
//            AvroSchemaMessageConverter converter = new AvroSchemaMessageConverter();
//            converter.setSchema(DomainEventEnriched.SCHEMA$);
//            return converter;
//        }

        @Bean
        public SchemaRegistryClient schemaRegistryClient(@Value("${spring.cloud.stream.schemaRegistryClient.endpoint}") String endpoint){
            ConfluentSchemaRegistryClient client = new ConfluentSchemaRegistryClient();
            client.setEndpoint(endpoint);
            return client;
        }
    }

}

interface KStreamProcessorX {

    String DOMAIN_EVENT_FEED = "domainEventFeed";
    String SOURCE_REFERENCE_DATA = "sourceReferenceData";
    String SOURCE_REFERENCE_STORE = "source-store";
    String DOMAIN_EVENT_ENRICHED_OUTPUT = "domainEventEnrichedOutput";
    String DOMAIN_EVENT_ENRICHED_FEED = "domainEventEnrichedFeed";

    @Input(DOMAIN_EVENT_FEED)
    KStream<String, DomainEvent> domainEventFeed();

    @Input(SOURCE_REFERENCE_DATA)
    KTable<String, SourceReferenceData> sourceReferenceData();

    @Output(DOMAIN_EVENT_ENRICHED_OUTPUT)
    KStream<String, DomainEventEnriched> domainEventEnrichedOutput();

    @Input(DOMAIN_EVENT_ENRICHED_FEED)
    SubscribableChannel domainEventEnrichedFeed();
}

