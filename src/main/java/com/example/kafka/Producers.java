package com.example.kafka;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class Producers {

    public static final String PAYMENTS = "PAY";
    public static final String ORDERS = "ORD";
    public static final String WAREHOUSE = "WAR";
    public static final String DELIVERY = "DEL";
    private static final String DOMAIN_EVENT_TOPIC = "global.domainEventFeed";
    private static final String SOURCE_REFERENCE_DATA_TOPIC = "global.sourceReferenceData";

    public static void main(String... args) {

        final Map<String, String> serdeConfig = Collections.singletonMap(
                AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        final SpecificAvroSerializer<SourceReferenceData> sourceReferenceDataSerializer = new SpecificAvroSerializer<>();
        sourceReferenceDataSerializer.configure(serdeConfig, false);

        Map<String, Object> props = new HashMap<>();
        props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
//        props.put(ProducerConfig.RETRIES_CONFIG, 0);
//        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
//        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
//        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, sourceReferenceDataSerializer.getClass());

        DefaultKafkaProducerFactory<String, SourceReferenceData> pfSourceReferenceData = new DefaultKafkaProducerFactory<>(props);
        KafkaTemplate<String, SourceReferenceData> templateSourceReferenceData = new KafkaTemplate<>(pfSourceReferenceData, true);
        templateSourceReferenceData.setDefaultTopic(SOURCE_REFERENCE_DATA_TOPIC);

        final List<SourceReferenceData> sourceReferenceDataList = Arrays.asList(
                SourceReferenceData.newBuilder().setIdentification(PAYMENTS).setDescription("Payments").build(),
                SourceReferenceData.newBuilder().setIdentification(ORDERS).setDescription("Orders").build(),
                SourceReferenceData.newBuilder().setIdentification(WAREHOUSE).setDescription("Warehouse").build(),
                SourceReferenceData.newBuilder().setIdentification(DELIVERY).setDescription("Delivery").build()
        );

        sourceReferenceDataList.forEach(sourceReferenceData -> {
            log.info("Writing sourceReferenceData for [{}]-[{}]", sourceReferenceData.getIdentification(), sourceReferenceData.getDescription());
            templateSourceReferenceData.sendDefault(sourceReferenceData.getIdentification(), sourceReferenceData);
        });

        final List<DomainEvent> domainEvents = Arrays.asList(
                DomainEvent.newBuilder().setEventType("Order Received").setUuid(UUID.randomUUID().toString()).setSource(ORDERS).build(),
                DomainEvent.newBuilder().setEventType("Payment received").setUuid(UUID.randomUUID().toString()).setSource(PAYMENTS).build(),
                DomainEvent.newBuilder().setEventType("Back ordered").setUuid(UUID.randomUUID().toString()).setSource(WAREHOUSE).build(),
                DomainEvent.newBuilder().setEventType("Back order received").setUuid(UUID.randomUUID().toString()).setSource(WAREHOUSE).build(),
                DomainEvent.newBuilder().setEventType("Order packed").setUuid(UUID.randomUUID().toString()).setSource(WAREHOUSE).build(),
                DomainEvent.newBuilder().setEventType("Order shipped").setUuid(UUID.randomUUID().toString()).setSource(WAREHOUSE).build(),
                DomainEvent.newBuilder().setEventType("Order delivered").setUuid(UUID.randomUUID().toString()).setSource(DELIVERY).build());

        final SpecificAvroSerializer<DomainEvent> domainEventSerializer = new SpecificAvroSerializer<>();
        domainEventSerializer.configure(serdeConfig, false);

        Map<String, Object> props1 = new HashMap<>(props);
        props1.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props1.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, domainEventSerializer.getClass());

        DefaultKafkaProducerFactory<String, DomainEvent> pfDomainEvents = new DefaultKafkaProducerFactory<>(props1);
        KafkaTemplate<String, DomainEvent> templateDomainEvents = new KafkaTemplate<>(pfDomainEvents, true);
        templateDomainEvents.setDefaultTopic(DOMAIN_EVENT_TOPIC);

        domainEvents.forEach(domainEvent -> {
            log.info("Writing domainEvent for [{}]-[{}]", domainEvent.getSource(), domainEvent.getEventType());
            templateDomainEvents.sendDefault(domainEvent.getSource(), domainEvent);
        });

    }

}